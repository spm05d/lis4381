# lis4381

## Sam Meador

### Assignment 5 Requirements:
1. Suitably modify index.php for A5
2. Turn off client-side validation
3. Add server-side validation and regular expressions
4. Connect A5 to petstore DB
5. Create sortable table to show data
6. Create page to add/edit/delete data to DB


#### README.md file should include the following items:

1. Screenshot of Data Table
2. Screenshot of Error page


#### Assignment Screenshots:

*Screenshot of data table*:

![Screenshot 1](img/data.png)

*Screenshot of error page*:

![Screenshot 2](img/error.png)

*Link to localhost website*:
http://localhost/repos/lis4381