> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Mobile Web Applications

## Samuel Meador

### Assignment 1 Requirements:

1. Install AMPPS
2. Install JDK
3. Install Android Studio and create My First App
4. Provide Screenshots of installations
5. Create Bitbucket Repo
6. Complete Bitbucket tutorials
7. Provide git command descriptions

#### README.md file should include the following items:

* Screenshot of AMPPS running
* Screenshot of running java *Hello*
* Screenshot of Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS running](img/AMPPS_running.PNG)

*Screenshot of running java Hello*: 

![java Hello screenshot](img/Hello.PNG)

*Screenshot of Android Studio - My First App

![Android Studio](img/MyFirstApp.PNG)

#### Git commands w/short descriptions:

1. git init: Creates an empty Git repository or reinitializes an existing one
2. git status: Shows the state of the working directory and the staging area
3. git add: Adds file contents to the index
4. git commit: Records changes to the repository
5. git push: Updates remote refs along with associated objects
6. git pull: Fetches from and integrates with another repository or a local branch
7. git clone: Clones a repository into a new directory

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/spm05d/bitbucketstationlocations "Bitbucket Station Locations")