# lis4381

## Sam Meador

### Project 2 Requirements:

1. Requires A5 cloned files.
2. Review subdirectories and files
3. Open index.php and review code:
    * Suitably modify meta tags
    * Change title, navigation links, and header tags appropriately
    * See videos for complete development.
    * Turn off client-side validation by commenting out the following code:<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>
    * Add server-sidevalidation and regular expressions--as per the database entity attribute requirements.

#### README.md file should include the following items:

* Screenshot of index.php for Project 2.
* Screenshot of edit_petstore.php.
* Screenshot of edit_petstore_process.php (that includes error.php).

#### Assignment Screenshots:

*Screenshot of index.php for Project 2*:

![Screenshot 1](img/index.png)

*Screenshot of edit_petstore.php*:

![Screenshot 2](img/edit.png)

*Screenshot of edit_petstore.php and error*:

![Screenshot 3](img/edit_error.png)

*Screenshot of RSS Feed*:

![Screenshot 4](img/rss.png)