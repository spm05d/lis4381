# lis4381

## Sam Meador

### Assignment 2 Requirements:

*Three:*

1. Create "home screen" for Bruchetta Recipe
2. Create secondary screen for instructions on how to make Bruchetta
3. Use Java to allow button to work with clicking

#### README.md file should include the following items:

* Screenshot of Screen 1
* Screenshot of Screen 2

#### Assignment Screenshots:

*Screenshot of Screen 1*:

![Screenshot 1](img/homescreen.png)

*Screenshot of Screen 2*:

![Screenshot 2](img/instructions.png)