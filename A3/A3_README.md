# lis4381

## Sam Meador

### Assignment 3 Requirements:

1. Create petstore DB 
2. Screenshot of running application’s first user interface 
3. Screenshot of running application’s second user interface 
4. Links to the following files:
..* a3.mwb
..* a3.sql

#### README.md file should include the following items:

* Screenshot of user interfaces
* Screenshot of ERD
* Link to .sql file
* Link to .mwb file

#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot 1](img/a3.png)

*Screenshot of User Interface 1*:

![Screenshot 2](img/firstuser.png)

*Screenshot of User Interface 2*:

![Screenshot 2](img/seconduser.png)

*Link to sql file*:
[A3 SQl File](docs/a3.sql "A3 SQL in .sql format")

*Link to mwb file*:
[A3 MWB File](docs/a3erd.mwb "A3 ERD in .mwb format")