> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

lis4381 Mobile App Development

Sam Meador

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](A1/A1_README.md "My A1 README.md file")

    1. Install AMPPS
    2. Install JDK
    3. Install Android Studio and create My First App
    4. Provide Screenshots of installations 
    5. Create Bitbucket Repo
    6. Complete Bitbucket tutorials
    7. Provide git command descriptions

2. [A2 README.md](A2/A2_README.md "My A2 README.md file")
    1. Create a mobile application with a working button
    2. Add TextView to application
    3. Add Image View to application
    4. Add working button
    5. Change text size

3. [A3 README.md](A3/A3_README.md "My A3 README.md file")
    1. Create petstore DB 
    2. Screenshot of running application’s first user interface 
    3. Screenshot of running application’s second user interface 
    4. Links to the following files:
        * a3.mwb
        * a3.sql

4. [A4 README.md](A4/A4_README.md "My A4 README.md file")
    1. Create repos folder in Ampps
    2. Clone Starter Student Files
    3. Modify index.php
        * Modify meta tags
        * Change title, navigation links, and header tags
        * Add form controls to match attributes of petstore entity
        * Add jQuery validation and regular expressions
    4. Add favicon using initials

5. [A5 README.md](A5/A5_README.md "My A5 README.md file")
    1. Suitably modify index.php for A5
    2. Turn off client-side validation
    3. Add server-side validation and regular expressions
    4. Connect A5 to petstore DB
    5. Create sortable table to show data
    6. Create page to add/edit/delete data to DB

6. [P1 README.md](P1/P1_README.md "My P1 README.md file")
    1. Add Headshot and Name with working button on Main Screen
    2. Add Contact Info with Interests to second screen.
    3. Headshot must have border
    4. Button must have shadow
    5. Button must take you to second screen with contact and interest details

7. [P2 README.md](P2/P2_README.md "My P2 README.md file")
    1. Requires A5 cloned files.
    2. Review subdirectories and files
    3. Open index.php and review code:
        * Suitably modify meta tags
        * Change title, navigation links, and header tags appropriately
        * See videos for complete development.
        * Turn off client-side validation by commenting out the following code:<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>
        * Add server-sidevalidation and regular expressions--as per the database entity attribute requirements.