# lis4381

## Sam Meador

### Assignment 4 Requirements:

1. Create repos folder in Ampps
2. Clone Starter Student Files
3. Modify index.php
    * Modify meta tags
    * Change title, navigation links, and header tags
    * Add form controls to match attributes of petstore entity
    * Add jQuery validation and regular expressions
4. Add favicon using initials

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of passed validation
* Link to localhost/repos/lis4381


#### Assignment Screenshots:

*Screenshot of failed validation*:

![Screenshot 1](img/failed.png)

*Screenshot of passed validation*:

![Screenshot 2](img/passed.png)

*Link to localhost website*:
http://localhost/repos/lis4381